function loadDatosTarifas(){
    let request = sendRequest('tarifas/list','GET','')
    let table = document.getElementById('tarifas-table')
    table.innerHTML = "";
    request.onload = function(){
        let datosTarifas = request.response
        datosTarifas.forEach(element => {
            table.innerHTML += `
            <tr class="table">
                <td>${element.idtarifas}</td>
                <td>${element.tipo_vehiculo}</td>
                <td>${element.tarifa}</td>
            </tr>
            `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
        <tr class="table">
                <td colspan="3">Error al recuperar los datos</td>
        </tr>
        `
    }
}

