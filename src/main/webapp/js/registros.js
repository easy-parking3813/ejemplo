function loadDatosRegistros() {
    let request = sendRequest('registro/list', 'GET', '')
    let table = document.getElementById('registros-table')
    table.innerHTML = "";
    request.onload = function () {
        let datosRegistros = request.response
        datosRegistros.forEach(element => {
            table.innerHTML += `
            <tr class="table">
                <td>${element.idregistro}</td>
                <td>${element.placa}</td>
                <td>${element.hora_ocupacion}</td>
                <td>${element.tipo_vehiculo}</td>
                <td>${element.piso}</td>
                <td>${element.hora_salida}</td>
                <td>${element.costo}</td>
                <td>
                    <button style="border-radius:20px;" type="button" class="btn btn-outline-success"
                    onclick='window.location="EP_formentrada.html?id=${element.idregistro}"' >Editar</button>
                    <button style="border-radius:20px; " type="button" class="btn btn-outline-danger"
                    onclick='deleteRegistro(${element.idregistro})'>Eliminar</button>
                </td>
            </tr>
            `
        });
    }
    request.onerror = function () {
        table.innerHTML = `
        <tr class="table">
                <td colspan="8">Error al recuperar los datos</td>
        </tr>
        `
    }
}

function deleteRegistro(idRegistro){
    let request = sendRequest('registro/'+idRegistro,'DELETE','');
    request.onload = function(){
        loadDatosRegistros();
    }
}


function saveRegistro(){
    let idregistro = document.getElementById('idregistro')
    let placa = document.getElementById('placa')
    let hora_ocupacion = document.getElementById('hora_ocupacion')
    let tipo_vehiculo = document.getElementById('tipo_vehiculo')
    let piso = coument.getElementById('piso')
    let hora_salida = document.getElementById('hora_salida')
    let costo = coument.getElementById('costo')
    let datosRegistros;
    (idregistro)
    ? datosRegistros = {idregistro: idregistro, placa: placa, hora_ocupacion:hora_ocupacion, tipo_vehiculo: tipo_vehiculo, piso: piso, hora_salida:hora_salida}
    : datosRegistros = {placa: placa, hora_ocupacion:hora_ocupacion, tipo_vehiculo: tipo_vehiculo, piso: piso, hora_salida:hora_salida}
    let request = sendRequest('registro/', (idregistro) ? 'PUT' : 'POST', datosRegistros)
    request.onload = function(){
        window.location = "EPregistros.html"
    }
    request.onerror= function(){
        alert('No se ha podido realizar la acción')
    }

}

function loadDatosRegistro(placa){
    let request = sendRequest('registro/'+placa, 'GET', '')
    let datosRegistro = request.response
    let table = document.getElementById('busqueda-table')
    request.onload = function(){
        table.innerHTML = `
        <tr class="table">
                <td id="placa">${datosRegistro.placa}</td>
                <td id="piso">${datosRegistro.piso}</td>
                <td id="hora_entrada">${datosRegistro.hora_ocupacion}</td>
                <td id="tipo_vehiculo">${datosRegistro.tipo_vehiculo}</td>
        </tr>
        `
    }
    request.onerror = function(){
        table.innerHTML = `
        <tr class="table">
                <th colspan=8>No se ha encontrado la placa digitada</th>
        </tr>
        `
    }
}
