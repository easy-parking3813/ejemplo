function loadDatosPlazas(){
    let request = sendRequest('plazas/list','GET','')
    let table = document.getElementById('plazas-table')
    table.innerHTML = "";
    request.onload = function(){
        let datosPlazas = request.response
        datosPlazas.forEach(element => {
            table.innerHTML += `
            <tr class="table">
                <td>${element.idplaza}</td>
                <td>${element.placa}</td>
                <td>${element.tipo_vehiculo}</td>
                <td>${element.piso}</td>
                <td>${element.hora_ocupacion}</td>
            </tr>
            `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
        <tr class="table">
                <td colspan="5">Error al recuperar los datos</td>
        </tr>
        `
    }
}