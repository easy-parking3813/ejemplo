package com.easyparking.frontend;


import com.easyparking.ticket.TicketSalida;
import com.misiontic.easyparking.FechaHora;
import com.misiontic.easyparking.RegistroBean;
import com.misiontic.easyparking.serviciosImp.PlazaServiciosImpl;
import com.misiontic.easyparking.serviciosImp.RegistroServiciosImpl;
import com.misiontic.easyparking.serviciosImp.TarifaServiciosImpl;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author dansan
 */
public class VentanaSalida extends javax.swing.JDialog {

    PlazaServiciosImpl dm = new PlazaServiciosImpl();
    RegistroServiciosImpl bdm = new RegistroServiciosImpl();
    TarifaServiciosImpl xbdm = new TarifaServiciosImpl();

    
    public VentanaSalida(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setVisible(true);
    }

    
    @SuppressWarnings("unchecked")    
    private void initComponents() {

        labelPlaca = new javax.swing.JLabel();
        campoPlaca = new javax.swing.JFormattedTextField();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        labelPlaca.setText("Placa");

        try {
            campoPlaca.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("UUU###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(labelPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(campoPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(119, 119, 119)
                                        .addComponent(btnAceptar)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnCancelar)))
                        .addContainerGap(37, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(labelPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(campoPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAceptar)
                                .addComponent(btnCancelar))
                        .addContainerGap(31, Short.MAX_VALUE))
        );

        pack();
    }            

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {
        //Compruebo si es una placa valida
        String placa = campoPlaca.getText();
        if (placa.length() == 6) {
            //Obtengo bean
            RegistroBean bean = dm.obtenerVehiculo(placa);

            //Fechas del ticket
            Date date = new Date();
            FechaHora fechahora = new FechaHora(date);
            String hora = fechahora.obtenerFechaHora();
            Date dateEntrada = new Date((long) bean.getHora_ocupacion());
            FechaHora fechaEntrada = new FechaHora(dateEntrada);
            String horaEntrada = fechaEntrada.obtenerFechaHora();

            //costo ,graba en registro y borro registro plazas
            double tarifa = xbdm.obtenerTarifas(bean.getTipo_vehiculo());
            double costo = calcularCosto(tarifa, dateEntrada.getTime(), date.getTime());
            bdm.grabarRegistro(placa, bean.getHora_ocupacion(), bean.getTipo_vehiculo(), bean.getPiso(), costo);
            dm.borrarPlazas(placa);

            //imprimo ticket
            imprimirTicket(horaEntrada, bean.getPiso(), bean.getTipo_vehiculo(), placa, hora, costo);
            JOptionPane.showMessageDialog(new JFrame(), "Operacion Satisfactoria", "", JOptionPane.INFORMATION_MESSAGE);
            this.dispose();
        }
    }

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {
        this.dispose();
    }

    private void imprimirTicket(String hora, int piso, char tipo, String placa, String hora_salida, double costo) {
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPrintable(new TicketSalida(hora, piso, tipo, placa, hora_salida, costo));
        PageFormat pageFormat = new PageFormat();
        pageFormat = job.pageDialog(pageFormat);
        if (job.printDialog()) {
            try {
                job.print();
            } catch (PrinterException ex) {
                Logger.getLogger(VentanaEntrada.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private double calcularCosto(double tarifa, long entrada, long salida){
        double tiempo= salida - entrada;
        double segundos = tiempo / 1000;
        double minutos = segundos / 60;
        double horas = minutos / 60;
        if (horas < 1) {
            horas = 1; // aunque sea se cobra una hora que es el minimo
        }
        double costo = tarifa * minutos;
        return costo;
    }
    
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JFormattedTextField campoPlaca;
    private javax.swing.JLabel labelPlaca;
}
