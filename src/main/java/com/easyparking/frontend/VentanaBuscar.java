package com.easyparking.frontend;

import com.misiontic.easyparking.RegistroBean;
import com.misiontic.easyparking.serviciosImp.PlazaServiciosImpl;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author dansan
 */
public class VentanaBuscar extends javax.swing.JDialog {

    PlazaServiciosImpl bdm = new PlazaServiciosImpl();

    public VentanaBuscar(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setVisible(true);
    }

    @SuppressWarnings("unchecked")    
    private void initComponents() {

        labelPlaca = new javax.swing.JLabel();
        labelPiso = new javax.swing.JLabel();
        campoPlaca = new javax.swing.JFormattedTextField();
        campoPiso = new javax.swing.JFormattedTextField();
        btnBuscarPlaca = new javax.swing.JButton();
        btnBuscarPiso = new javax.swing.JButton();
        scrollTabla = new javax.swing.JScrollPane();
        tablaVehiculos = new javax.swing.JTable();
        btnCerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        labelPlaca.setText("Placa");

        labelPiso.setText("Piso");

        try {
            campoPlaca.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("UUU###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            campoPiso.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        btnBuscarPlaca.setText("Buscar");
        btnBuscarPlaca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarPlacaActionPerformed(evt);
            }
        });

        btnBuscarPiso.setText("Buscar");
        btnBuscarPiso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarPisoActionPerformed(evt);
            }
        });

        tablaVehiculos.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{
                    {null, null, null, null}
                },
                new String[]{
                    "Placa", "Piso", "Hora de Ingreso", "Tipo de Vehiculo"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
        });
        scrollTabla.setViewportView(tablaVehiculos);

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(scrollTabla, javax.swing.GroupLayout.DEFAULT_SIZE, 732, Short.MAX_VALUE)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(labelPlaca, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                                                .addComponent(labelPiso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(campoPlaca)
                                                .addComponent(campoPiso, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(btnBuscarPlaca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(btnBuscarPiso, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE))
                                        .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(77, 77, 77))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(labelPlaca)
                                .addComponent(campoPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnBuscarPlaca))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(labelPiso)
                                .addComponent(campoPiso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnBuscarPiso))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(scrollTabla, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                        .addComponent(btnCerrar))
        );

        pack();
    }

    
    private void btnBuscarPlacaActionPerformed(java.awt.event.ActionEvent evt) {
        if (campoPlaca.getText().trim().length()==6) {
            RegistroBean bean = bdm.obtenerVehiculo(campoPlaca.getText());            
            Double d2 = bean.getHora_ocupacion();
            Date dateEntrada = new Date(d2.longValue());
            DefaultTableModel tm33 = (DefaultTableModel) tablaVehiculos.getModel();
            tm33.setRowCount(0);
            if (bean.getPiso()>0) {
                Object rowData[] = {bean.getPlaca(), bean.getPiso(), dateEntrada.toString(), bean.getTipo_vehiculo()};
                tm33.addRow(rowData);
            }
        }
    }

    private void btnBuscarPisoActionPerformed(java.awt.event.ActionEvent evt) {
        if (!campoPiso.getText().equals("  ")) {
            int piso = Integer.parseInt(campoPiso.getText());
            ArrayList<RegistroBean> beanes = bdm.obtenerVehiculo(piso);
            if (beanes.size() > 0) {
                DefaultTableModel tm33 = (DefaultTableModel) tablaVehiculos.getModel();
                tm33.setRowCount(0);
                for (int i = 0; i < beanes.size(); i++) {
                    RegistroBean bean = beanes.get(i);
                    Double d2 = bean.getHora_ocupacion();
                    Date dateEntrada = new Date(d2.longValue());
                    Object rowData[] = {bean.getPlaca(), bean.getPiso(), dateEntrada.toString(), bean.getTipo_vehiculo()};
                    tm33.addRow(rowData);
                }
            }
        }
    }

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {
        this.dispose();
    }
                      
    private javax.swing.JButton btnBuscarPlaca;
    private javax.swing.JButton btnBuscarPiso;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JFormattedTextField campoPlaca;
    private javax.swing.JFormattedTextField campoPiso;
    private javax.swing.JLabel labelPlaca;
    private javax.swing.JLabel labelPiso;
    private javax.swing.JScrollPane scrollTabla;
    private javax.swing.JTable tablaVehiculos;    
}
