/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.easyparking.interfaces;

import com.misiontic.easyparking.modelo.Plaza;
import org.springframework.data.repository.CrudRepository;
/**
 *
 * @author wrmed
 */
public interface PlazaDao extends CrudRepository<Plaza, Integer>{
    
}
