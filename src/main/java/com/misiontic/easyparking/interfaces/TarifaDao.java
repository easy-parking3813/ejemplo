/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.easyparking.interfaces;

import com.misiontic.easyparking.modelo.Tarifa;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author PERSONAL
 */
public interface TarifaDao extends CrudRepository<Tarifa, Integer>{
    
}
