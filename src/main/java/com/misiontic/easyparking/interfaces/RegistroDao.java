/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.easyparking.interfaces;

import com.misiontic.easyparking.modelo.Registro;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author wrmed
 */
public interface RegistroDao extends CrudRepository<Registro, Integer>{
    
    @Query(nativeQuery = true, value = "select * from registro where placa = ?")
    public Registro findByPlate(String plate);
}
