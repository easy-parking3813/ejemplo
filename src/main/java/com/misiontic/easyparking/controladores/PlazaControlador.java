/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.easyparking.controladores;

import com.misiontic.easyparking.modelo.Plaza;
import com.misiontic.easyparking.servicios.PlazaServicios;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wrmed
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/plaza")
public class PlazaControlador {
    
    @Autowired
    private PlazaServicios plazaServicios;
    
    @GetMapping(value = "/list")
    public List<Plaza> listar(){
        return plazaServicios.findAll();
    }
    
    @GetMapping(value = "/{id}")
    public Plaza consultarPorId(@PathVariable Integer id){
        return plazaServicios.findById(id);
    }
    
    @PostMapping(value = "/")
    public ResponseEntity<Plaza> agregar(@RequestBody Plaza nueva){
        Plaza obj = plazaServicios.save(nueva);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Plaza> eleminar(@PathVariable Integer id){
        Plaza obj = plazaServicios.findById(id);
        if( obj != null){
            plazaServicios.delete(id);
        }else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value = "/")
    public ResponseEntity<Plaza> editar(@RequestBody Plaza plaza){
        Plaza obj = plazaServicios.findById(plaza.getIdplaza());
        if( obj != null){
            obj.setPlaca(plaza.getPlaca());
            obj.setHora_ocupacion(plaza.getHora_ocupacion());
            obj.setTipo_vehiculo(plaza.getTipo_vehiculo());
            obj.setPiso(plaza.getPiso());
            plazaServicios.save(obj);
        }else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    
}
