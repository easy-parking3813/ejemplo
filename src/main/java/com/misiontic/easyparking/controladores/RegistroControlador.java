/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.easyparking.controladores;

import com.misiontic.easyparking.modelo.Registro;
import com.misiontic.easyparking.servicios.RegistroServicios;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wrmed
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/registro")
public class RegistroControlador {
    
    @Autowired
    private RegistroServicios registroServicios;
    
    @GetMapping(value = "/list")
    public List<Registro> listar(){
        return registroServicios.findAll();
    }
    
    @GetMapping(value = "/{id}")
    public Registro consultarPorId(@PathVariable Integer id){
        return registroServicios.findById(id);
    }
    
    @GetMapping(value = "/{placa}")
    public Registro consultarPorPlaca(@PathVariable String placa){
        return registroServicios.findByPlate(placa);
    }
    
    @PostMapping(value = "/")
    public ResponseEntity<Registro> agregar(@RequestBody Registro nueva){
        Registro obj = registroServicios.save(nueva);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Registro> eleminar(@PathVariable Integer id){
        Registro obj = registroServicios.findById(id);
        if( obj != null){
            registroServicios.delete(id);
        }else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value = "/")
    public ResponseEntity<Registro> editar(@RequestBody Registro registro){
        Registro obj = registroServicios.findById(registro.getIdregistro());
        if( obj != null){
            obj.setPlaca(registro.getPlaca());
            obj.setHora_ocupacion(registro.getHora_ocupacion());
            obj.setTipo_vehiculo(registro.getTipo_vehiculo());
            obj.setPiso(registro.getPiso());
            obj.setHora_ocupacion(registro.getHora_ocupacion());
            obj.setCosto(registro.getCosto());
            registroServicios.save(obj);
        }else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    //@PutMapping(value="/exit")
    //public ResponseEntity<Registro> salida(@RequestBody Registro registro){
       // Registro obj = registroServicios.findByPlate(registro.getPlaca());
        
        //if(obj.getHora_ocupacion() != null){
          //  String date_format = "dd-mm-YY HH:mm";
            //DateFormat formatter = new SimpleDateFormat(date_format);
            //String formattedDate = formatter.format(new Date);
            //obj.setHora_salida();
            
    //    }
   //}
}

