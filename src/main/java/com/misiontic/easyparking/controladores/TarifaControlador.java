/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.easyparking.controladores;

import com.misiontic.easyparking.modelo.Tarifa;
import com.misiontic.easyparking.servicios.TarifaServicios;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PERSONAL
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/tarifa")
public class TarifaControlador {
    
    @Autowired
    private TarifaServicios tarifaServicios;
    
    @GetMapping(value = "/list")
    public List<Tarifa> listar(){
        return tarifaServicios.findAll();
    }
    
    @GetMapping(value = "/{id}")
    public Tarifa consultarPorId(@PathVariable Integer id){
        return tarifaServicios.findById(id);
    }
    
    @PostMapping(value = "/")
    public ResponseEntity<Tarifa> agregar(@RequestBody Tarifa nueva){
        Tarifa obj = tarifaServicios.save(nueva);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Tarifa> eleminar(@PathVariable Integer id){
        Tarifa obj = tarifaServicios.findById(id);
        if( obj != null){
            tarifaServicios.delete(id);
        }else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value = "/")
    public ResponseEntity<Tarifa> editar(@RequestBody Tarifa tarifa){
        Tarifa obj = tarifaServicios.findById(tarifa.getIdtarifas());
        if( obj != null){
            obj.setTipo_vehiculo(tarifa.getTipo_vehiculo());
            obj.setTarifa(tarifa.getTarifa());
            
            tarifaServicios.save(obj);
        }else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    
}
