package com.misiontic.easyparking;

/**
 *
 * @author dansan
 */
public class RegistroBean {

    private String placa;
    private double hora_ocupacion;
    private char tipo_vehiculo;
    private int piso;
    private double hora_salida;
    private double costo;

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the hora_ocupacion
     */
    public double getHora_ocupacion() {
        return hora_ocupacion;
    }

    /**
     * @param hora_ocupacion the hora_ocupacion to set
     */
    public void setHora_ocupacion(double hora_ocupacion) {
        this.hora_ocupacion = hora_ocupacion;
    }

    /**
     * @return the tipo_vehiculo
     */
    public char getTipo_vehiculo() {
        return tipo_vehiculo;
    }

    /**
     * @param tipo_vehiculo the tipo_vehiculo to set
     */
    public void setTipo_vehiculo(char tipo_vehiculo) {
        this.tipo_vehiculo = tipo_vehiculo;
    }

    /**
     * @return the piso
     */
    public int getPiso() {
        return piso;
    }

    /**
     * @param piso the piso to set
     */
    public void setPiso(int piso) {
        this.piso = piso;
    }

    /**
     * @return the hora_salida
     */
    public double getHora_salida() {
        return hora_salida;
    }

    /**
     * @param hora_salida the hora_salida to set
     */
    public void setHora_salida(double hora_salida) {
        this.hora_salida = hora_salida;
    }

    /**
     * @return the costo
     */
    public double getCosto() {
        return costo;
    }

    /**
     * @param costo the costo to set
     */
    public void setCosto(double costo) {
        this.costo = costo;
    }

}
