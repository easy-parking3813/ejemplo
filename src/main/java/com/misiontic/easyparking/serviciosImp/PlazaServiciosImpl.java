/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.easyparking.serviciosImp;

import com.misiontic.easyparking.RegistroBean;
import com.misiontic.easyparking.interfaces.PlazaDao;
import com.misiontic.easyparking.modelo.Plaza;
import com.misiontic.easyparking.servicios.PlazaServicios;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 *
 * @author wrmed
 */
@Service
public class PlazaServiciosImpl implements PlazaServicios{

    private String username = "root";
    private String password = "";
    private String db = "jdbc:mysql://localhost:3306/bdparqueadero";
    private String driver = "com.mysql.jdbc.Driver";
    private Connection conexion;
    private Statement st;

    @Autowired
    private PlazaDao plazaDao;
    
    @Override
    public List<Plaza> findAll() {
        return (List<Plaza>) plazaDao.findAll();
    }

    @Override
    public void delete(Integer id) {
        plazaDao.deleteById(id);
    }

    @Override
    public Plaza findById(Integer id) {
        return plazaDao.findById(id).orElse(null);
    }

    @Override
    public Plaza save(Plaza nuevaPlaza) {
        return plazaDao.save(nuevaPlaza);
    }
    
public PlazaServiciosImpl() {
        try {
            Class.forName(driver);
            conexion = DriverManager.getConnection(db, username, password);
            st = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ResultSet listarPlazas() {
        ResultSet rs = null;
        String pedido = "select * from plazas";
        try {
            rs = st.executeQuery(pedido);
        } catch (SQLException ex) {
            Logger.getLogger(PlazaServiciosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public int numVehiculosEnPlazas(int piso) {
        int total = 0;
        ResultSet rs=null;
        String pedido = "select count(*) as total from plazas where piso="+piso;
        try {
            rs = st.executeQuery(pedido);
            while(rs.next()){
                total = rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PlazaServiciosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }
    
    
    public RegistroBean obtenerVehiculo(String placa) {
        RegistroBean bean = new RegistroBean();
        try {
            String query = "select * from plazas where placa='" + placa + "'";
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                bean.setPlaca(rs.getString("placa"));
                bean.setHora_ocupacion(rs.getDouble("hora_ocupacion"));
                String cad = (String) rs.getObject("tipo_vehiculo");
                char c = cad.charAt(0);
                bean.setTipo_vehiculo(c);
                bean.setPiso(rs.getInt("piso"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(PlazaServiciosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bean;
    }

    public ArrayList<RegistroBean> obtenerVehiculo(int piso) {
        ArrayList<RegistroBean> beanes = new ArrayList<RegistroBean>();
        try {
            String query = "select * from plazas where piso=" + piso;
            ResultSet rs = st.executeQuery(query);
            rs.beforeFirst();
            while (rs.next()) {
                RegistroBean bean = new RegistroBean();
                bean.setPlaca(rs.getString("placa"));
                bean.setHora_ocupacion(rs.getDouble("hora_ocupacion"));
                String cad = (String) rs.getObject("tipo_vehiculo");
                char c = cad.charAt(0);
                bean.setTipo_vehiculo(c);
                bean.setPiso(rs.getInt("piso"));
                beanes.add(bean);
            }

        } catch (SQLException ex) {
            Logger.getLogger(PlazaServiciosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return beanes;
    }

    public void agregarPlazas(String placa, int piso, char tipo) {
        Date date = new Date();
        double fecha = date.getTime();
        String query = "insert into plazas (placa,hora_ocupacion,tipo_vehiculo,piso) ";
        query += "values('" + placa + "','" + fecha + "','" + tipo + "','" + piso + "')";
        try {
            st.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(PlazaServiciosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void borrarPlazas(String placa) {
        String query = "delete from plazas where placa = '" + placa + "'";
        try {
            st.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(PlazaServiciosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
