/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.easyparking.serviciosImp;

import com.misiontic.easyparking.interfaces.TarifaDao;
import com.misiontic.easyparking.modelo.Tarifa;
import com.misiontic.easyparking.servicios.TarifaServicios;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author PERSONAL
 */
@Service
public class TarifaServiciosImpl implements TarifaServicios{

    private String username = "root";
    private String password = "";
    private String db = "jdbc:mysql://localhost:3306/bdparqueadero";
    private String driver = "com.mysql.jdbc.Driver";
    private Connection conexion;
    private Statement st;

    @Autowired
    private TarifaDao tarifaDao;
    
    @Override
    public List<Tarifa> findAll() {
        return (List<Tarifa>) tarifaDao.findAll();
    }

    @Override
    public void delete(Integer id) {
        tarifaDao.deleteById(id);
    }

    @Override
    public Tarifa findById(Integer id) {
        return tarifaDao.findById(id).orElse(null);
    }

    @Override
    public Tarifa save(Tarifa nuevaTarifa) {
        return tarifaDao.save(nuevaTarifa);
    }
    
public TarifaServiciosImpl() {
        try {
            Class.forName(driver);
            conexion = DriverManager.getConnection(db, username, password);
            st = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public double obtenerTarifas(char c){

        double tarifa = 10;
        String pedido = "select * from tarifa where tipo_vehiculo='" + c + "'";
        try {
            ResultSet rs = st.executeQuery(pedido);
            if (rs.next()) {
                tarifa = (double) rs.getObject("tarifa");
            }
        } catch (SQLException ex) {
            Logger.getLogger(TarifaServiciosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tarifa;
    }

    public ResultSet listarTarifas() {
        ResultSet rs = null;
        String pedido = "select * from tarifa";
        try {
            rs = st.executeQuery(pedido);
        } catch (SQLException ex) {
            Logger.getLogger(TarifaServiciosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

}
