/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.easyparking.serviciosImp;

import com.misiontic.easyparking.interfaces.RegistroDao;
import com.misiontic.easyparking.modelo.Registro;
import com.misiontic.easyparking.servicios.RegistroServicios;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wrmed
 */
@Service
public class RegistroServiciosImpl implements RegistroServicios{

    private String username = "root";
    private String password = "";
    private String db = "jdbc:mysql://localhost:3306/bdparqueadero";
    private String driver = "com.mysql.jdbc.Driver";
    private Connection conexion;
    private Statement st;

    @Autowired
    private RegistroDao registroDao;
    
    @Override
    public List<Registro> findAll() {
        return (List<Registro>) registroDao.findAll();
    }

    @Override
    public void delete(Integer id) {
        registroDao.deleteById(id);
    }

    @Override
    public Registro findById(Integer id) {
        return registroDao.findById(id).orElse(null);
    }

    @Override
    public Registro save(Registro nuevaRegistro) {
        return registroDao.save(nuevaRegistro);
    }
    
public RegistroServiciosImpl() {
        try {
            Class.forName(driver);
            conexion = DriverManager.getConnection(db, username, password);
            st = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ResultSet listarRegistro() {
        ResultSet rs = null;
        String query = "select * from registro";
        try {
            rs = st.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(RegistroServiciosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public void grabarRegistro(String placa, double hora_ocupacion, char tipo, int piso, double costo) {
        Date date = new Date();
        double fecha = date.getTime();
        String query = "";
        query += "insert into registro (placa,hora_ocupacion,tipo_vehiculo,piso,hora_salida,costo) ";
        query += "values('" + placa + "','" + hora_ocupacion + "','" + tipo + "','" + piso + "','" + fecha + "','" + costo + "')";
        try {
            st.executeUpdate(query);
        } catch (SQLException ex) {
            Logger.getLogger(RegistroServiciosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Registro findByPlate(String plate) {
        return registroDao.findByPlate(plate);
    }

    
}
