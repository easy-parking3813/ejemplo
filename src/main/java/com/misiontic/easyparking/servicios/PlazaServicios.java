/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.easyparking.servicios;

import com.misiontic.easyparking.modelo.Plaza;
import java.util.List;

/**
 *
 * @author wrmed
 */
public interface PlazaServicios {
    public List<Plaza> findAll();
    public void delete(Integer id);
    public Plaza findById(Integer id);
    public Plaza save(Plaza plaza);
}
