/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.easyparking.servicios;

import com.misiontic.easyparking.modelo.Tarifa;
import java.util.List;

/**
 *
 * @author PERSONAL
 */
public interface TarifaServicios {
    
    public List<Tarifa> findAll();
    public void delete(Integer id);
    public Tarifa findById(Integer id);
    public Tarifa save(Tarifa tarifa);
    
}
