/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.easyparking.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 *
 * @author wrmed
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "registro")
public class Registro implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idregistro")
    private Integer idregistro;
    
    @Column(name = "placa")
    private String placa;
    
    @Column(name = "hora_ocupacion")
    private String hora_ocupacion;
    
    @Column(name = "tipo_vehiculo")
    private String tipo_vehiculo;
    
    @Column(name = "piso")
    private double piso;
    
    @Column(name = "hora_salida")
    private String hora_salida;
    
    @Column(name = "costo")
    private String costo;
    
}

